package org.spy.dropwizard.module;

import org.spy.dropwizard.SampleConfiguration;

import com.google.inject.PrivateModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public abstract class ConfigurableServiceModule extends PrivateModule{
	
	private SampleConfiguration sampleConfiguration;

	@Provides
	@Singleton
	public SampleConfiguration getSampleConfiguration() {
		return sampleConfiguration;
	}

	public void setSampleConfiguration(SampleConfiguration sampleConfiguration) {
		this.sampleConfiguration = sampleConfiguration;
	}
	
	

}
