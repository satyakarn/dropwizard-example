package org.spy.dropwizard.module;

import org.spy.dropwizard.SampleConfiguration;
import org.spy.dropwizard.health.SampleHealthCheck;
import org.spy.dropwizard.resource.SampleResource;

import com.google.inject.Provides;

import io.dropwizard.setup.Environment;

public class SampleServiceModule extends ConfigurableServiceModule{
	
	private final Environment environment;
	
	public SampleServiceModule(final SampleConfiguration sampleConfiguration, final Environment environment) {
		setSampleConfiguration(sampleConfiguration);
		this.environment = environment;
	}
	
	@Override
	public void configure() {
		bind(SampleResource.class);
		expose(SampleResource.class);
		bind(SampleHealthCheck.class);
		expose(SampleHealthCheck.class);
	}
	
	@Provides
	public Environment getEnvironment() {
		return environment;
	}

}
