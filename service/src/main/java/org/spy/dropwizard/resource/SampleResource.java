package org.spy.dropwizard.resource;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Sample Resource
 * @author satya
 *
 */
@Path("/sample")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("Sample Resource")
public class SampleResource {
	
	private final Logger logger = LoggerFactory.getLogger(SampleResource.class);
	
	@Path("/time")
	@ApiOperation("Endpoint to get current time")
	@GET
    public String getCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.s");
        String currentTime = formatter.format(GregorianCalendar.getInstance().getTime());
        logger.info("Current Time: {}", currentTime);
        
        return currentTime;
    }

}
