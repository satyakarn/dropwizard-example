package org.spy.dropwizard.health;

import com.codahale.metrics.health.HealthCheck;

public class SampleHealthCheck extends HealthCheck {
    
	@Override
    protected Result check() throws Exception {
        if (true) {
            return Result.healthy();
        } else {
            return Result.unhealthy("Cannot connect to some resource");
        }
    }
}
