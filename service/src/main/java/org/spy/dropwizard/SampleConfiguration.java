package org.spy.dropwizard;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.bundles.assets.AssetsBundleConfiguration;
import io.dropwizard.bundles.assets.AssetsConfiguration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class SampleConfiguration extends Configuration implements AssetsBundleConfiguration{
    @Valid
    @NotNull
    @JsonProperty("message")
    private String message;
    
    @JsonProperty("swagger")
    private SwaggerBundleConfiguration swaggerBundleConfiguration;
    
    @JsonProperty("assets")
    private AssetsConfiguration assets;

    public String getMessage() {
        return message;
    }
    
    public SwaggerBundleConfiguration getSwaggerBundleConfiguration() {
    	return swaggerBundleConfiguration;
    }

    @Override
	public AssetsConfiguration getAssetsConfiguration() {
		return assets;
	}
}
