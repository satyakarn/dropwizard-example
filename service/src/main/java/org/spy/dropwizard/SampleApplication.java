package org.spy.dropwizard;

import org.spy.dropwizard.health.SampleHealthCheck;
import org.spy.dropwizard.module.SampleServiceModule;
import org.spy.dropwizard.resource.SampleResource;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

import io.dropwizard.Application;
import io.dropwizard.bundles.assets.ConfiguredAssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class SampleApplication extends Application<SampleConfiguration> {
	
	private static final String URI_BASE_PATH = "/sample-ui";
    
    @Override
    public void initialize(Bootstrap<SampleConfiguration> bootstrap) {
    	
    	// Enable swagger
    	bootstrap.addBundle(new SwaggerBundle<SampleConfiguration>() {
			@Override
			protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(SampleConfiguration configuration) {
				return configuration.getSwaggerBundleConfiguration();
			}
		});
    	
        // Enable variable substitution with environment variables
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                                                   new EnvironmentVariableSubstitutor(false)
                )
        );
        
        bootstrap.addBundle(new ConfiguredAssetsBundle("/sample-ui/", URI_BASE_PATH, "hello.html"));
    }

	@Override
	public void run(SampleConfiguration configuration, Environment environment) throws Exception {		
		final Injector injector = Guice.createInjector(Stage.PRODUCTION, new SampleServiceModule(configuration, environment));
		
		// encapsulate complicated setup logic in factories
	    environment.jersey().register(injector.getInstance(SampleResource.class));
	    environment.healthChecks().register("sample-resource", injector.getInstance(SampleHealthCheck.class));
	}
	
	public static void main(String[] args) throws Exception {
		new SampleApplication().run(args);
	}
}
